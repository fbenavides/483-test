﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cantidad1 = new System.Windows.Forms.TextBox();
            this.tot1 = new System.Windows.Forms.TextBox();
            this.tot2 = new System.Windows.Forms.TextBox();
            this.precio2 = new System.Windows.Forms.TextBox();
            this.cantidad2 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.tot3 = new System.Windows.Forms.TextBox();
            this.precio3 = new System.Windows.Forms.TextBox();
            this.cantidad3 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.tot4 = new System.Windows.Forms.TextBox();
            this.precio4 = new System.Windows.Forms.TextBox();
            this.cantidad4 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lerror = new System.Windows.Forms.Label();
            this.lmsg = new System.Windows.Forms.Label();
            this.precio1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descripcion";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cantidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(414, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Precio c.u.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(521, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(34, 74);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(216, 22);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // cantidad1
            // 
            this.cantidad1.Location = new System.Drawing.Point(256, 74);
            this.cantidad1.Name = "cantidad1";
            this.cantidad1.Size = new System.Drawing.Size(66, 22);
            this.cantidad1.TabIndex = 5;
            this.cantidad1.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // tot1
            // 
            this.tot1.Enabled = false;
            this.tot1.Location = new System.Drawing.Point(501, 74);
            this.tot1.Name = "tot1";
            this.tot1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tot1.Size = new System.Drawing.Size(66, 22);
            this.tot1.TabIndex = 7;
            // 
            // tot2
            // 
            this.tot2.Enabled = false;
            this.tot2.Location = new System.Drawing.Point(501, 114);
            this.tot2.Name = "tot2";
            this.tot2.Size = new System.Drawing.Size(66, 22);
            this.tot2.TabIndex = 11;
            // 
            // precio2
            // 
            this.precio2.Location = new System.Drawing.Point(413, 114);
            this.precio2.Name = "precio2";
            this.precio2.Size = new System.Drawing.Size(66, 22);
            this.precio2.TabIndex = 10;
            // 
            // cantidad2
            // 
            this.cantidad2.Location = new System.Drawing.Point(256, 114);
            this.cantidad2.Name = "cantidad2";
            this.cantidad2.Size = new System.Drawing.Size(66, 22);
            this.cantidad2.TabIndex = 9;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(34, 114);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(216, 22);
            this.textBox8.TabIndex = 8;
            // 
            // tot3
            // 
            this.tot3.Enabled = false;
            this.tot3.Location = new System.Drawing.Point(501, 156);
            this.tot3.Name = "tot3";
            this.tot3.Size = new System.Drawing.Size(66, 22);
            this.tot3.TabIndex = 15;
            // 
            // precio3
            // 
            this.precio3.Location = new System.Drawing.Point(413, 156);
            this.precio3.Name = "precio3";
            this.precio3.Size = new System.Drawing.Size(66, 22);
            this.precio3.TabIndex = 14;
            // 
            // cantidad3
            // 
            this.cantidad3.Location = new System.Drawing.Point(256, 156);
            this.cantidad3.Name = "cantidad3";
            this.cantidad3.Size = new System.Drawing.Size(66, 22);
            this.cantidad3.TabIndex = 13;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(34, 156);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(216, 22);
            this.textBox12.TabIndex = 12;
            // 
            // tot4
            // 
            this.tot4.Enabled = false;
            this.tot4.Location = new System.Drawing.Point(501, 200);
            this.tot4.Name = "tot4";
            this.tot4.Size = new System.Drawing.Size(66, 22);
            this.tot4.TabIndex = 19;
            // 
            // precio4
            // 
            this.precio4.Location = new System.Drawing.Point(413, 200);
            this.precio4.Name = "precio4";
            this.precio4.Size = new System.Drawing.Size(66, 22);
            this.precio4.TabIndex = 18;
            // 
            // cantidad4
            // 
            this.cantidad4.Location = new System.Drawing.Point(256, 200);
            this.cantidad4.Name = "cantidad4";
            this.cantidad4.Size = new System.Drawing.Size(66, 22);
            this.cantidad4.TabIndex = 17;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(34, 200);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(216, 22);
            this.textBox16.TabIndex = 16;
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(501, 253);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(66, 22);
            this.textBox17.TabIndex = 20;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(501, 297);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(66, 22);
            this.textBox18.TabIndex = 21;
            // 
            // textBox19
            // 
            this.textBox19.Enabled = false;
            this.textBox19.Location = new System.Drawing.Point(501, 336);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(66, 22);
            this.textBox19.TabIndex = 22;
            // 
            // textBox20
            // 
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(501, 379);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(66, 22);
            this.textBox20.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(493, 425);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(73, 34);
            this.button1.TabIndex = 24;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lerror
            // 
            this.lerror.AutoSize = true;
            this.lerror.Location = new System.Drawing.Point(31, 336);
            this.lerror.Name = "lerror";
            this.lerror.Size = new System.Drawing.Size(64, 17);
            this.lerror.TabIndex = 25;
            this.lerror.Text = "Cantidad";
            // 
            // lmsg
            // 
            this.lmsg.AutoSize = true;
            this.lmsg.Location = new System.Drawing.Point(31, 302);
            this.lmsg.Name = "lmsg";
            this.lmsg.Size = new System.Drawing.Size(64, 17);
            this.lmsg.TabIndex = 26;
            this.lmsg.Text = "Cantidad";
            // 
            // precio1
            // 
            this.precio1.Location = new System.Drawing.Point(415, 74);
            this.precio1.Name = "precio1";
            this.precio1.Size = new System.Drawing.Size(63, 22);
            this.precio1.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 485);
            this.Controls.Add(this.precio1);
            this.Controls.Add(this.lmsg);
            this.Controls.Add(this.lerror);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.tot4);
            this.Controls.Add(this.precio4);
            this.Controls.Add(this.cantidad4);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.tot3);
            this.Controls.Add(this.precio3);
            this.Controls.Add(this.cantidad3);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.tot2);
            this.Controls.Add(this.precio2);
            this.Controls.Add(this.cantidad2);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.tot1);
            this.Controls.Add(this.cantidad1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox cantidad1;
        private System.Windows.Forms.TextBox tot1;
        private System.Windows.Forms.TextBox tot2;
        private System.Windows.Forms.TextBox precio2;
        private System.Windows.Forms.TextBox cantidad2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox tot3;
        private System.Windows.Forms.TextBox precio3;
        private System.Windows.Forms.TextBox cantidad3;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox tot4;
        private System.Windows.Forms.TextBox precio4;
        private System.Windows.Forms.TextBox cantidad4;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lerror;
        private System.Windows.Forms.Label lmsg;
        private System.Windows.Forms.TextBox precio1;
    }
}

