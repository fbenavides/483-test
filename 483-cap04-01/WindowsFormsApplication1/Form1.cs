﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int cant1 = 0;
            int cant2 = 0;
            int cant3 = 0;
            int cant4 = 0;

            decimal prec1 = 0;
            decimal prec2 = 0;
            decimal prec3 = 0;
            decimal prec4 = 0;

            lerror.Text = "";
            lmsg.Text = "";

            try
            {
                if (cantidad1.Text != "")
                {
                    cant1 = int.Parse(cantidad1.Text);
                    //prec1 = decimal.Parse(precio1.Text, NumberStyles.Currency);
                    //if(!decimal.TryParse(precio1.Text, NumberStyles.AllowCurrencySymbol,
                    if (!decimal.TryParse("$123", NumberStyles.AllowCurrencySymbol,
                        null, out prec1))
                    {
                        // Complain.
                        lerror.Text = "Invalid format. Price Each must be an currency value.";
                    }
                    tot1.Text = Convert.ToString(cant1 * prec1);
                }
                if (cantidad2.Text != "")
                {
                    cant2 = Convert.ToUInt16(cantidad2.Text);
                    prec2 = decimal.Parse(precio2.Text, NumberStyles.AllowCurrencySymbol);
                    tot2.Text = Convert.ToString(cant2 * prec2);
                }
                if (cantidad3.Text != "")
                {
                    cant3 = Convert.ToUInt16(cantidad3.Text);
                    prec3 = decimal.Parse(precio3.Text, NumberStyles.AllowCurrencySymbol);
                    tot3.Text = Convert.ToString(cant3 * prec3);
                }
                if (cantidad4.Text != "")
                {
                    cant4 = Convert.ToUInt16(cantidad4.Text);
                    prec4 = decimal.Parse(precio4.Text, NumberStyles.AllowCurrencySymbol);
                    tot4.Text = Convert.ToString(cant4 * prec4);
                }
            }
            catch( Exception exc )
            {
                lerror.Text = exc.Message;
            }
            
                

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
