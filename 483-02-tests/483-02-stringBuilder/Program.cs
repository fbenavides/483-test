﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _483_02_stringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder abecedario = new StringBuilder("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            StringBuilder imprimir = new StringBuilder();
            for(int i = 0; i < abecedario.Length; i++)
            {
                imprimir.Append(abecedario[i]);
                Console.WriteLine(imprimir);
            }
            Console.Read();
        }
    }
}
