﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _483_03_structs
{
    class Program
    {
        public struct book
        {
            public string title;
            public string category;
            public string author;
            public ushort numberPages;
            public ushort currentPage;
            public string isbn;
            public string coverStyle;

            public book(string title, string category, string author, ushort numberPages, ushort currentPage, string isbn, string coverStyle)
            {
                this.title = title;
                this.category = category;
                this.author = author;
                this.numberPages = numberPages;
                this.currentPage = currentPage;
                this.isbn = isbn;
                this.coverStyle = coverStyle;
            }

            public void nextPage()
            {
                if (this.currentPage < this.numberPages) ;
                    this.currentPage++;
                this.printCurrentPage();
            }

            public void prevPage()
            {
                if (this.currentPage > 1) ;
                    this.currentPage--;
                this.printCurrentPage();
            }

            private void printCurrentPage()
            {
                Console.WriteLine("Current page is: " + this.currentPage);
            }


        }
        static void Main(string[] args)
        {
            book myBook = new book("PHP","Development","Fabio",899,1,"1234567890","Dark");

            Console.WriteLine("Struct info");

            Console.WriteLine("Name: " + myBook.title);
            Console.WriteLine("Category: " + myBook.category );
            Console.WriteLine("Author: " + myBook.author);
            Console.WriteLine("Total of pages: " + myBook.numberPages);
            Console.WriteLine("Current page: " + myBook.currentPage);
            Console.WriteLine("Cover style: " + myBook.coverStyle);

            myBook.nextPage();
            myBook.nextPage();
            myBook.nextPage();
            myBook.nextPage();
            myBook.nextPage();

            myBook.prevPage();
            myBook.prevPage();
            myBook.prevPage();

            myBook.nextPage();
            myBook.nextPage();
            myBook.prevPage();
            
            Console.ReadKey();
        }
    }
}
