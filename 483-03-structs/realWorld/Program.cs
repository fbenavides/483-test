﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace realWorld
{
    class Student
    {
        public string firstName;
        public string lastName;
        public string schoolName;
        public short grade;

        public Student()
        {
            Console.WriteLine("Constructor empty");
        }

        public Student(string first, string last)
        {
            firstName = first;
            lastName = last;
            Console.WriteLine("Constructor 2 parameters");
        }

        public Student(string first, string last, short schoolGrade) : this(first, last)
        {
            grade = schoolGrade;
            Console.WriteLine("Constructor three parameters");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student noConst = new Student();
            Student withTwoPara = new Student("fabio", "benavides");
            Student withThree = new Student("fabio", "benavides", 9);
            Console.ReadKey();
        }
    }
}
