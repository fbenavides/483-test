﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ref_tests
{
    class Student
    {
        public string firstName;
        public string lastName;
        public string grade;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student firstStudent = new Student();
            firstStudent.firstName = "John";
            firstStudent.lastName = "Smith";
            firstStudent.grade = "six";

            Student secondStudent = new Student();
            secondStudent.firstName = "fabio";
            secondStudent.lastName = "benavides";
            secondStudent.grade = "first";

            changeRefs(firstStudent, secondStudent);

            Console.Write(firstStudent.firstName);

            Console.ReadKey();

        }

        static void changeRefs(Student per1, Student per2)
        {
            Student temp;
            temp = per1;
            per1 = per2;
            per2 = temp;
        }
    }
}
