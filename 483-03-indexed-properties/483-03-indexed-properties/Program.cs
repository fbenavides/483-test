﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _483_03_indexed_properties
{
    class Program
    {
        public class IPAddress
        {
            private int[] ip = new int[32];
            public int this[int index]
            {
                get
                {
                    return ip[index];
                }
                set
                {
                    if (value == 0 || value == 1)
                        ip[index] = value;
                    else
                        throw new Exception("Invalid value");
                }
            }
        }

        static void Main(string[] args)
        {
            IPAddress myIP = new IPAddress();
            // initialize the IP address to all zeros
            for (int i = 0; i < 32; i++)
            {
                myIP[i] = 0;

            }
            Console.WriteLine(myIP[7]);
            Console.ReadLine();
        }
    }
}
