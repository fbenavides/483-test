﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GenericsExample
{
    [Serializable]
    [XmlRootAttribute("Item")]
    public  class Car
    {
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Brand { get; set; }
        public string Something { get; set; }
        public Car() { }
    }
}
