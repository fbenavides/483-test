﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GenericsExample
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(System.Configuration.ConfigurationManager.AppSettings["xmlpath"]);
            string XMLPerson = doc.InnerXml;
            Person person = Util.ConvertToTModel<Person>(XMLPerson);          
            doc.Load(System.Configuration.ConfigurationManager.AppSettings["xmlpath"]);
            string XMLCar = doc.InnerXml;
            Car car = Util.ConvertToTModel<Car>(XMLCar);


        }
    }
}
