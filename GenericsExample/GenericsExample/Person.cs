﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GenericsExample
{
   // [Serializable]
    [XmlRootAttribute("Item")]
    public class Person
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string Status { get; set; }


        public Person() { }
    }
}
