﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace GenericsExample
{
    public class Util
    {
        public Util() { }

        public static T ConvertToTModel<T>(string xml)
        {
            T newObjectType = default(T);
            try
            {
                XmlReaderSettings setting = new XmlReaderSettings();
                setting.CheckCharacters = false;
                setting.ValidationType = ValidationType.Schema;
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                TextReader reader = new StringReader(xml);
                newObjectType = (T)serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return newObjectType;
        }
    }
}
