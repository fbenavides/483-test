﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _483_varios_test
{
    class Program
    {

        private enum monthNames
        {
            Enero = 1, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Setiembre, Octubre, Noviembre, Diciembre
        };


        static void Main()
        {
            string day = DateTime.Now.Day.ToString("00");
            string month = GetMonth(DateTime.Now.Month);
            string month2 = GetMonthEnum(DateTime.Now.Month);
            int year = DateTime.Now.Year;
            string time = DateTime.Now.ToLongTimeString();

            Console.WriteLine("Today is {0} of {1} of {2}. It´s {3}.", day, month, year, time);
            Console.WriteLine("Today ienum s {0} of {1} of {2}. It´s {3}.", day, month2, year, time);
            Console.ReadKey();
        }

        static string GetMonth(int numberMonth)
        {
            string[] months =
                new string[] { "January", "February", "March", "April", "May", "June", "July",
                           "August", "September", "October", "November", "Decenber" };

            return months[numberMonth - 1];
        }

        static string GetMonthEnum(int numberMonth)
        {
            switch (numberMonth)
            {
                case (int)monthNames.Enero:
                    return "Enero";
                case (int)monthNames.Febrero:
                    return "Febrero";
                case (int)monthNames.Marzo:
                    return "Marzo";
                case (int)monthNames.Abril:
                    return "Abril";
                case (int)monthNames.Mayo:
                    return "Mayo";
                case (int)monthNames.Junio:
                    return "Junio";
                case (int)monthNames.Julio:
                    return "Julio";
                case (int)monthNames.Agosto:
                    return "Agosto";
                case (int)monthNames.Setiembre:
                    return "Setiembre";
                case (int)monthNames.Octubre:
                    return "Octubre";
                case (int)monthNames.Noviembre:
                    return "Noviembre";
                default:
                    return "Diciembre";
            }
        }
    }
}
