﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace reflexion
{
    class Meeting : INotifyPropertyChanged
    {
        private string _title;
        private string _description;
        private DateTime _start;
        private DateTime _end;

        public Meeting() { }
        public Meeting(string title)
        {
            _title = title;
        }

        public DateTime Start
        {
            set
            {
                _start = value;
            }
            get
            {
                return _start;
            }
        }
        public DateTime End
        {
            set
            {
                _end = value;
            }
            get
            {
                return _end;
            }
        }
        public string Title
        {
            set
            {
                _title = value;
            }
            get
            {
                return _title;
            }
        }
        public string Description
        {
            set
            {
                _description = value;
            }
            get
            {
                return _description;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        public DateTime CalcDuration()
        {
            return DateTime.Now;
        }
        public DateTime CalcRemainder()
        {
            return DateTime.Now;
        }
    }
}
