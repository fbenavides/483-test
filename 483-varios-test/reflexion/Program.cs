﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace reflexion
{
    class Program
    {
        static void Main(string[] args)
        {
            Type t;

            //si tenemos instancias
            int i = 5;
            t = i.GetType();
            Console.WriteLine(t); // imprime 'System.Int32'

            string s = "hola mundo";
            t = s.GetType();
            Console.WriteLine(t); // imprime 'System.String'

            Meeting m = new Meeting();
            t = m.GetType();
            Console.WriteLine(t); // imprime 'Calendars.Meeting'

            //si no tenemos instancias
            t = typeof(int);
            Console.WriteLine(t); // imprime 'System.Int32'

            t = typeof(String);
            Console.WriteLine(t); // imprime 'System.String'

            t = typeof(Meeting);
            Console.WriteLine(t); // imprime 'Calendars.Meeting'

            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
                Console.WriteLine(type);



            Console.ReadKey();
        }
    }
}
