﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegado
{
   public delegate void NotifyFunc(string msg);
    public class Employee
    {
        public Employee(string name, int age)
        {
            _name = name;
            _age = age;
        }

        public Employee()
            : this(string.Empty, 0)
        {
        }

        public event NotifyFunc Notify;

        protected virtual void OnNotify(string msg)
        {
            if (Notify != null)
                Notify(msg);
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnNotify("Name ha cambiado, nuevo valor: " + value);
                }
            }
        }

        public int Age
        {
            get { return _age; }
            set
            {
                if (_age != value)
                {
                    _age = value;
                    OnNotify("Age ha cambiado, nuevo valor: " + value);
                }
            }
        }

        private string _name;
        private int _age;
    }

    static class Program
    {
        static void Print(string msg)
        {
            Console.WriteLine("Recibiendo mensaje: " + msg);
        }

        static void Main(string[] args)
        {
            Employee emp = new Employee();
            emp.Notify += Print;
            emp.Name = "Fernando Gómez. ";
            emp.Age = 28;

            Console.ReadKey(true);
        }
    }

}
