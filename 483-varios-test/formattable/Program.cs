﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace formattable
{
    class Program
    {
        public static void Main()
        {
            //CultureInfo.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            Temperature temp1 = new Temperature(22m);
            Console.WriteLine(Convert.ToString(temp1, new CultureInfo("ja-JP")));
            Console.WriteLine("Temperature: {0:K}", temp1);
            Console.WriteLine("Temperature: {0:F}", temp1);
            Console.WriteLine(String.Format(new CultureInfo("fr-FR"), "Temperature: {0:F}", temp1));
            Console.WriteLine(temp1.ToString());
            Console.ReadKey();
        }
    }
}
