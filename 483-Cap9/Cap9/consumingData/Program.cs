﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consumingData
{
    class Program
    {
        static void Main(string[] args)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=cap9test;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;

            Random rnd = new Random();
            int id = rnd.Next(99999);

            cmd.CommandText = "INSERT INTO Persona (Id, Nombre, Apellido) " +
                              "VALUES ("+ id +", 'Joe"+id+"', 'Smith"+id+"')";
            cmd.ExecuteNonQuery();


            cmd.CommandText = "SELECT * FROM Persona";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Console.WriteLine(string.Format("First Name: {0} , Last Name: {1}",
                                                  dr["Nombre"], dr["Apellido"]));
                }
            }

            Console.ReadKey();

            dr.Close();
            cn.Close();
        }
    }
}
