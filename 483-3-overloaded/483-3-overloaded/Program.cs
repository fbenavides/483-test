﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _483_3_overloaded
{
    class Program
    {
        class fabio
        {
            public double sumar(double num1, double num2)
            {
                return num1 + num2;
            }

            public float sumar(float num1, float num2)
            {
                return num1 + num2;
            }
        }

        static void Main(string[] args)
        {
            fabio f = new fabio();

            Console.WriteLine(f.sumar(123,456));
        }
    }
}
