﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegate_one
{
    class Program
    {
        

        static void Main(string[] args)
        {
            MyClass miObjeto = new MyClass();

            miObjeto.LongRun(Callback);

            Console.ReadLine();
        }

        static void Callback(int x)
        {
            Console.WriteLine(x);
        }

        static void Callback2(int x)
        {
            Console.WriteLine("callback 2");
        }

        

    }

    public class MyClass
    {
        //public delegate void CallBack(int x);
        public Action<int> CallBack;

        public void LongRun(Action<int> myFunction)
        {
            for(int i = 0; i < 1000; i++)
            {
                //hacer algo
                myFunction.Invoke(i);
            }
        }
    }
}
