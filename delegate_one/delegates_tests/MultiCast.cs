﻿using System;

namespace delegates_tests
{
    public class MultiCast
    {
        public delegate void del();

        public void MethodOne()
        {
            Console.WriteLine("Method One");
        }

        public void MethodTwo()
        {
            Console.WriteLine("Method Two");
        }

        public MultiCast()
        {
            del d = MethodOne;
            d += MethodTwo;

            d();
        }
    }
}
