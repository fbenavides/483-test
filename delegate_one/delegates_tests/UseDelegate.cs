﻿using System;

namespace delegates_tests
{
    class UseDelegate
    {
        public delegate int Calculator(int x, int y);

        public int Sumar(int x, int y)
        {
            return x + y;
        }

        public int Multi(int x, int y)
        {
            return x * y;
        }

        public UseDelegate()
        {
            Calculator calc = Sumar;

            Func<int, int , int > miTest = Sumar; 

            Console.WriteLine(calc(3,5));
            Console.WriteLine(miTest(3, 3));
            
            calc = Multi;

            Console.WriteLine(calc(3, 5));


            miTest =  (int x, int y) => x + y;
            Console.WriteLine(miTest(4, 4));

        }
    }
}
