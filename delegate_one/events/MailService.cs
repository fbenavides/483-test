﻿using System;

namespace events
{
    public class MailService
    {
        public void OnVideoEncoded(object source, VideoEventArgs vid)
        {
            Console.WriteLine("Mailservice: sending an email...{0}", vid.Video.Name);
        }
    }
}
