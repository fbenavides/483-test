﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace events
{
    class VideoEncoder
    {
        // 1- Define a delegate
        // 2 - Define an event base on that delegate
        // 3 - Raise the event

        //1
        public delegate void VideoEncodedEventHandler(object source, VideoEventArgs args);

        //2
        public event VideoEncodedEventHandler VideoEncoded;

        //3
        protected virtual void OnVideoEncoded(Video video)
        {
            if(VideoEncoded != null)
            {
                //VideoEncoded(this, EventArgs.Empty);
                VideoEncoded(this, new VideoEventArgs() { Video = video } );
            }
        }

        public void Encode(Video video)
        {
            Console.WriteLine("Encoding...");
            Thread.Sleep(3000);

            OnVideoEncoded(video);

            Console.ReadLine();
        }
    }
}
