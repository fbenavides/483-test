﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events
{
    class Program
    {
        static void Main(string[] args)
        {
            Video video = new Video("mi video de prueba");
            VideoEncoder videoEncoder = new VideoEncoder();
            MailService mailServices = new MailService();
            TextService textServie = new TextService();

            videoEncoder.VideoEncoded += mailServices.OnVideoEncoded;
            videoEncoder.VideoEncoded += textServie.OnVideoEncoded;


            videoEncoder.Encode(video);
        }
    }
}
