﻿using System;

namespace events
{
    public class TextService
    {
        public void OnVideoEncoded(object source, VideoEventArgs vid)
        {
            Console.WriteLine("TextService: sending a text...{0}", vid.Video.Name);
        }
    }
}
