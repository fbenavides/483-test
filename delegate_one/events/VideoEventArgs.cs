﻿using System;

namespace events
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }
}
